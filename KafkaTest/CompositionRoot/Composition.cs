﻿using Abstractions;
using EventConsumer.Kafka;
using EventProducer.Kafka;
using Listener1;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;

namespace CompositionRoot
{
    public class Composition
    {
        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            //var producerOptions = new ProducerOptions();
            //configuration.Bind(ProducerOptions.SectionName, producerOptions);
            //services.AddSingleton(producerOptions);

            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));

            services.AddScoped<IEventProducer, EventProducer.Kafka.EventProducer>();

            // event listener
            var consumerOptions = new ConsumerOptions();
            configuration.Bind(ConsumerOptions.SectionName, consumerOptions);
            services.AddSingleton(consumerOptions);

            services.AddTransient<IEventConsumer, EventConsumer.Kafka.EventConsumer>();
        }
    }
}
