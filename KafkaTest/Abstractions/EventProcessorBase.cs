﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Abstractions
{
    public abstract class EventProcessorBase<T> : IEventProcessor
        where T : EventBase
    {
        public Task HandleAsync(EventBase @event) => HandleAsync(@event as T);

        protected abstract Task HandleAsync(T @event);
    }
}
