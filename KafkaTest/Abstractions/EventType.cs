﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstractions
{
    public enum EventType
    {
        BookingCreatedEvent = 0,
        BookingDateRangeUpdatedEvent = 1,
        BookingStatusUpdatedEvent = 2,
        UserCreatedEvent = 3,
        UserUpdatedEvent = 4,
        UserStatusUpdatedEvent = 5,
        SpaceCreatedEvent = 6,
        SpaceUpdatedEvent = 7,
        SpaceStatusUpdatedEvent = 8,
        UserAttributeCreatedEvent = 9,
        UserAttributeUpdetedEvent = 10,
        UserAttributeDeletedEvent = 11,
        RoleCreatedEvent = 12,
        RoleUpdatedEvent = 13,
        RoleDeletedEvent = 14,
        UserRoleCreatedEvent = 15,
        UserRoleDeletedEvent = 16
    }
}
