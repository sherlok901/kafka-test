﻿using System;

namespace Abstractions
{
    public static class EventProcessorFactory
    {
        public delegate IEventProcessor Create(EventType message);
    }
}
