﻿using System.Threading;
using System.Threading.Tasks;

namespace Listener1
{
    public interface IEventConsumer
    {
        Task ConsumeAsync(CancellationTokenSource cts);
    }
}
