﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Abstractions
{
    public interface IEventProducer
    {
        Task Publish<T>(T @event)
            where T : EventBase;
    }
}
