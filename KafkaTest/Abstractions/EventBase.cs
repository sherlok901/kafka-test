﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstractions
{
    public abstract class EventBase
    {
        public Guid CorrelationId { get; set; }
    }
}
