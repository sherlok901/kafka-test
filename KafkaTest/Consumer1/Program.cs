﻿using Abstractions;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using CompositionRoot;
using Events;
using Listener1;
using Listener1.Processors.Booking;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.IO;
using System.Threading;

namespace Consumer1
{
    class Program
    {
        public static IServiceProvider ServiceProvider;

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException; ;

            InitializeDependencyInjection();

            var cts = new CancellationTokenSource();

            //on break
            Console.CancelKeyPress += (s, e) =>
            {
                e.Cancel = true; // prevent the process from terminating.
                cts.Cancel();
            };

            var consumer = ServiceProvider.GetService<IEventConsumer>();
            consumer.ConsumeAsync(cts).GetAwaiter().GetResult();

            Console.ReadKey();

            //Console.WriteLine("Hello World!");
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Logger.Error(e.ExceptionObject?.ToString());
        }

        private static void InitializeDependencyInjection()
        {
            var services = new ServiceCollection();

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            IConfiguration configuration = builder.Build();
            services.AddOptions();
            services.AddSingleton<IHostingEnvironment>(x => new HostingEnvironment { ApplicationName = "EventListener.Kafka" });

            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();

            Composition.Register(services, configuration);

            services.AddTransient<EventProcessorFactory.Create>(serviceProvider => key => key switch
            {
                EventType.BookingCreatedEvent => serviceProvider.GetService<BookingCreatedEventProcessor>(),
                EventType.BookingDateRangeUpdatedEvent => serviceProvider.GetService<BookingDateRangeUpdatedEventProcessor>(),
                EventType.BookingStatusUpdatedEvent => serviceProvider.GetService<BookingStatusUpdatedEventProcessor>(),
   
            });
            var autoFacBuilder = new ContainerBuilder();
            autoFacBuilder.Populate(services);
            ServiceProvider = new AutofacServiceProvider(autoFacBuilder.Build());
        }
    }
}
