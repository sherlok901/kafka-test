﻿using Abstractions;
using Events;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EventConsumer.Kafka
{
    public class EventDeserializer
    {
        public EventType DeserializeEventType(byte[] eventTypeData)
        {
            return (EventType)BitConverter.ToInt32(eventTypeData, 0);
        }

        public EventBase DeserializeEvent(byte[] eventData, EventType eventType)
        {
            var jsonValue = Encoding.UTF8.GetString(eventData);
            return JsonConvert.DeserializeObject(jsonValue, eventType.GetMessageClassType()) as EventBase;
        }
    }
}
