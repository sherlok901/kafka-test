﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventConsumer.Kafka
{
    public class ConsumerOptions
    {
        public static string SectionName => "Brokers";

        public string BrokerEndpoints { get; set; }


        public IDictionary<string, string> Options { get; set; }

        public IEnumerable<string> Topics { get; set; }
    }
}
