﻿using Abstractions;
using Confluent.Kafka;
using Listener1;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventConsumer.Kafka
{
    public class EventConsumer : IEventConsumer
    {
        private readonly ConsumerOptions options;
        private readonly ILogger logger;
        private readonly EventProcessorFactory.Create eventProcessorFactory;
        private readonly EventDeserializer eventDeserializer = new EventDeserializer();

        public EventConsumer(ConsumerOptions options, ILogger<EventConsumer> logger,
            EventProcessorFactory.Create eventProcessorFactory)
        {
            this.options = options;
            this.logger = logger;
            this.eventProcessorFactory = eventProcessorFactory;
        }

        public async Task ConsumeAsync(CancellationTokenSource cts)
        {
            var consumerBuilder = new ConsumerBuilder<Null, byte[]>(options.Options)
                .SetKeyDeserializer(Deserializers.Null)
                .SetValueDeserializer(Deserializers.ByteArray);

            using (var consumer = consumerBuilder.Build())
            {
                consumer.Subscribe(options.Topics);

                try
                {
                    while (!cts.IsCancellationRequested)
                    {
                        try
                        {
                            logger.LogInformation("Start");

                            var consumeResult = consumer.Consume(cts.Token);
                            if (!consumeResult.Message.Headers.TryGetLastBytes("type", out var type))
                            {
                                throw new Exception("Message header bytes not gotten");
                            }

                            //get event type
                            var eventType = eventDeserializer.DeserializeEventType(type);
                            var eventProcessor = eventProcessorFactory(eventType);
                            var messageObj = eventDeserializer.DeserializeEvent(consumeResult.Value, eventType);

                            try
                            {
                                await eventProcessor?.HandleAsync(messageObj);
                            }
                            catch (Exception e)
                            {
                                logger.LogError(e, "Message handling error");
                                throw;
                            }

                            logger.LogInformation($"Consumed message '{messageObj}' at: '{consumeResult?.TopicPartitionOffset}'.");
                        }
                        catch (ConsumeException e)
                        {
                            logger.LogError($"Error occurred: {e.Error.Reason}");
                            throw;
                        }
                    }
                }
                catch (OperationCanceledException)
                {
                    // Ensure the consumer leaves the group cleanly and final offsets are committed.
                    consumer.Close();
                }
            }
        }
    }
}
