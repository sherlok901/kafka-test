﻿using Abstractions;
using Events;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EventProducer.Kafka
{
    public class EventSerializer
    {
        public byte[] SerializeEvent(EventBase @event)
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(@event));
        }

        public byte[] SerializeEventType(EventBase @event)
        {
            return BitConverter.GetBytes((int)@event.GetType().GetEventEnumType());
        }
    }
}
