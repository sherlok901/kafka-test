﻿using Abstractions;
using Confluent.Kafka;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace EventProducer.Kafka
{
    public class EventProducer : IEventProducer
    {
        private readonly ProducerOptions producerOptions;
        private readonly ILogger logger;
        private readonly ProducerBuilder<Null, byte[]> producerBuilder;
        private readonly EventSerializer eventSerializer = new EventSerializer();

        public EventProducer(ProducerOptions producerOptions, ILogger<EventProducer> logger)
        {
            this.producerOptions = producerOptions;
            this.logger = logger;

            producerBuilder = new ProducerBuilder<Null, byte[]>(new ProducerConfig { BootstrapServers = producerOptions.BrokerEndpoints, Acks = Acks.All })
                .SetKeySerializer(Serializers.Null)
                .SetValueSerializer(Serializers.ByteArray);
        }

        public async Task Publish<T>(T @event) where T : EventBase
        {
            using (var producer = producerBuilder.Build())
            {
                //json
                var message = new Message<Null, byte[]> { Value = eventSerializer.SerializeEvent(@event) };
                message.Headers = new Headers();
                message.Headers.Add("type", eventSerializer.SerializeEventType(@event));

                try
                {
                    var deliveryResult = await producer.ProduceAsync(topic: producerOptions.Topic, message);
                    //todo: use Polly for retry
                    //Install-Package Polly
                }
                catch (Exception e)
                {
                    logger.LogError($"Error occurred: {e}");
                    throw;
                }
            }
        }
    }
}
