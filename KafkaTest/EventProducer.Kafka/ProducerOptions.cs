﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventProducer.Kafka
{
    public class ProducerOptions
    {
        public static string SectionName => "Brokers";

        public string Topic { get; set; }

        public string BrokerEndpoints { get; set; }
    }
}
