﻿using Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Events
{
    public class BookingStatusUpdatedEvent : EventBase
    {
        public Guid Id { get; set; }

        public int StatusId { get; set; }
    }
}
