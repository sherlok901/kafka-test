﻿using Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Events
{
    public class BookingCreatedEvent : EventBase
    {
        public Guid BookingId { get; set; }

        public Guid? UserId { get; set; }

        public Guid SpaceId { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }
    }
}
