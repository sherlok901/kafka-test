﻿using Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Events
{
    public class BookingDateRangeUpdatedEvent : EventBase
    {
        public Guid Id { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }
    }
}
