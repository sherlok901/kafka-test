﻿using Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Events
{
    public static class EventTypeExtensions
    {
        private static readonly Dictionary<EventType, Type> EventTypeToType = new Dictionary<EventType, Type>
        {
            [EventType.BookingCreatedEvent] = typeof(BookingCreatedEvent),
            [EventType.BookingDateRangeUpdatedEvent] = typeof(BookingDateRangeUpdatedEvent),
            [EventType.BookingStatusUpdatedEvent] = typeof(BookingStatusUpdatedEvent),
            //[EventType.UserCreatedEvent] = typeof(UserCreatedEvent),
            //[EventType.UserUpdatedEvent] = typeof(UserUpdatedEvent),
            //[EventType.UserStatusUpdatedEvent] = typeof(UserStatusUpdatedEvent),
            //[EventType.SpaceCreatedEvent] = typeof(SpaceCreatedEvent),
            //[EventType.SpaceUpdatedEvent] = typeof(SpaceUpdatedEvent),
            //[EventType.SpaceStatusUpdatedEvent] = typeof(SpaceStatusUpdatedEvent),
            //[EventType.UserAttributeCreatedEvent] = typeof(UserAttributeCreatedEvent),
            //[EventType.UserAttributeUpdetedEvent] = typeof(UserAttributeUpdetedEvent),
            //[EventType.UserAttributeDeletedEvent] = typeof(UserAttributeDeletedEvent),
            //[EventType.RoleCreatedEvent] = typeof(RoleCreatedEvent),
            //[EventType.RoleUpdatedEvent] = typeof(RoleUpdatedEvent),
            //[EventType.RoleDeletedEvent] = typeof(RoleDeletedEvent),
            //[EventType.UserRoleCreatedEvent] = typeof(UserRoleCreatedEvent),
            //[EventType.UserRoleDeletedEvent] = typeof(UserRoleDeletedEvent),
        };

        private static readonly Dictionary<Type, EventType> TypeToEventType = EventTypeToType.ToDictionary(i => i.Value, y => y.Key);

        public static Type GetMessageClassType(this EventType eventType)
        {
            if (!EventTypeToType.TryGetValue(eventType, out var result))
            {
                throw new ArgumentException($"Invalid event type {eventType}", nameof(eventType));
            }

            return result;
        }

        public static EventType GetEventEnumType(this Type eventType)
        {
            if (!TypeToEventType.TryGetValue(eventType, out var result))
            {
                throw new ArgumentException($"Invalid event type {eventType}", nameof(eventType));
            }

            return result;
        }
    }
}
